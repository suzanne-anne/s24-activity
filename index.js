console.log("Hello");

// declare a function that takes a number as input and returns its cube
function getCube(num) {
  return num ** 3;
}

const cubeOfTwo = getCube(2);
console.log(`The cube of 2 is ${cubeOfTwo}.`);













// create an array with details of the address
const address = [622, 'Angeles', 'Pampanga', 'Philippines', '2009'];
console.log(`I live at ${address[0]} ${address[1]} ${address[2]} ${address[3]} ${address[4]}.`);











// create an object with details of the animal
const animal = {
  name: 'Lolong',
  species: 'saltwater crocodile',
  weight: '1075 kg',
  length: {
    feet: 20,
    inches: 3
  }
};

const { name, species, weight, length } = animal;
const { feet, inches } = length;

// print a message to the console with the formatted animal details
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${feet} ft ${inches} in.`);









// create an array of numbers
const numbers = [1, 2, 3, 4, 5];
numbers.forEach(number => console.log(number));

const reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(reduceNumber);















// define the Dog class
class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}


const myDog = new Dog('Fido', 3, 'Labrador');
console.log(myDog);
